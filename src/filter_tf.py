
import rospy
import tf
import tf.msg
import geometry_msgs.msg
import math
from tf import TransformListener
import numpy as np
from collections import deque

class DynamicTFBroadcaster:
	def __init__(self):
		self.rate = rospy.Rate(10)
		self.pub_tf = rospy.Publisher("/denso_cube_tf", tf.msg.tfMessage, queue_size=10)
		self.tf_listener = TransformListener()
		self.t = geometry_msgs.msg.TransformStamped()
		self.tf_array = np.zeros((4), dtype=np.float64)
		self.size = 10
		self.buffer = deque(maxlen=self.size)
		self.mainloop()
	
	# function auxiliar para calcular a media
	def mean(self, _list, size):
		i = 0
		for i in _list:
			i = i + _list
		return i/size

	# function filtro de media
	# params
	# array buffer
	# int size
	def meanFilter(self, buffer, size):
		list_x = list()
		list_y = list()
		list_z = list()
		list_w = list()
		for element in buffer:
			list_x.append(element[0])
			list_y.append(element[1])
			list_z.append(element[2])
			list_w.append(element[3])
		
		mean_x = self.mean(list_x, size)
		mean_y = self.mean(list_y, size)
		mean_z = self.mean(list_z, size)
		mean_w = self.mean(list_w, size)

		mean = [mean_x, mean_y, mean_z, mean_w]

		return mean

	# function filtro de mediana
	# params
	# array buffer
	# int size
	def medianFilter(self, buffer, size):
		list_x = list()
		list_y = list()
		list_z = list()
		list_w = list()
		for element in buffer:
			list_x.append(element[0])
			list_y.append(element[1])
			list_z.append(element[2])
			list_w.append(element[3])
		
		list_x.sort()
		list_y.sort()
		list_z.sort()
		list_w.sort()

		median_x = list_x[size/2]
		median_y = list_y[size/2]
		median_z = list_z[size/2]
		median_w = list_w[size/2]

		median = [median_x, median_y, median_z, median_w]

		return median

	def mainloop(self):
		print("Starting the program.")
		print("Buffering......")

		while not rospy.is_shutdown():
			# Run this loop at about 10Hz
			#self.rate.sleep()
			
			if self.tf_listener.frameExists("denso") and self.tf_listener.frameExists("ar_marker_0"):
					
					rospy.sleep(0.5)
					
					try:
						position, quaternion = self.tf_listener.lookupTransform("denso", "ar_marker_0", rospy.Time(0))
					except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
						continue

					self.t.header.frame_id = "denso"
					self.t.header.stamp = rospy.Time.now()
					self.t.child_frame_id = "ar_marker_0"
				
					self.t.transform.rotation.x = quaternion[0]
					self.t.transform.rotation.y = quaternion[1]
					self.t.transform.rotation.z = quaternion[2]

					# function round(x, 3) para arredondar os valores com 3 casas decimais
					self.tf_array[0] = round(position[0], 3)
					self.tf_array[1] = round(position[1], 3)
					self.tf_array[2] = round(position[2], 3)
					self.tf_array[3] = round(quaternion[3], 3)

					# adiciona os valores de tf no buffer
					self.buffer.append(self.tf_array)

					# espera que o buffer esteja completo para iniciar a filtragem e publication
					if(len(self.buffer) == self.size):

						# chama o filtro na linha abaixo
						self.translation_array = self.medianFilter(self.buffer, self.size)
						
						# iguala o vetor de translation aos valores retornados do filtro
						self.t.transform.translation.x = self.tf_array[0]
						self.t.transform.translation.y = self.tf_array[1]
						self.t.transform.translation.z = self.tf_array[2]

						self.t.transform.rotation.w = self.tf_array[3]

						# publica
						tfm = tf.msg.tfMessage([self.t])
						self.pub_tf.publish(tfm)
						rospy.loginfo(tfm)

if __name__ == '__main__':
	rospy.init_node('denso_cube_tf_broadcaster')
	tfb = DynamicTFBroadcaster()
	rospy.spin()


