#include "ros/ros.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Pose.h"
#include "ar_track_alvar_msgs/AlvarMarkers.h"

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/message_filter.h"

#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>

#include <cstdlib>

using namespace std;

bool has_c920, has_c270;
geometry_msgs::Pose pose_c920, pose_c270;

void cb_c920(ar_track_alvar_msgs::AlvarMarkers req) {
    if (!req.markers.empty()) {

        for (uint i = 0; i <req.markers.size(); ++i )
        {
            if (req.markers[i].id == 0)
            {
                has_c920 = true;
                pose_c920 = req.markers[i].pose.pose;
            }
        }
        //      tf::Quaternion q(req.markers[0].pose.pose.orientation.x, req.markers[0].pose.pose.orientation.y, req.markers[0].pose.pose.orientation.z, req.markers[0].pose.pose.orientation.w);
        //      tf::Matrix3x3 m(q);
        //      double roll, pitch, yaw;
        //      m.getRPY(roll, pitch, yaw);
        //      ROS_INFO("roll, pitch, yaw=%1.2f  %1.2f  %1.2f", roll, pitch, yaw);
        // roll  --> rotate around vertical axis
        // pitch --> rotate around horizontal axis
        // yaw   --> rotate around depth axis
        //cout << req <<endl;
    }
}
void cb_c270(ar_track_alvar_msgs::AlvarMarkers req) {
    if (!req.markers.empty()) {
        for (uint i = 0; i <req.markers.size(); ++i )
        {
            if (req.markers[i].id == 0)
            {
                has_c270 = true;
                pose_c270 = req.markers[i].pose.pose;
            }
        }
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "arlistener");
    ros::NodeHandle nh;
    ros::Subscriber sub_c920 = nh.subscribe("c920/ar_pose_marker", 1, cb_c920);
    ros::Subscriber sub_c270 = nh.subscribe("c270/ar_pose_marker", 1, cb_c270);

    tf2_ros::TransformBroadcaster br;
    geometry_msgs::TransformStamped transformStamped;

    ros::Rate rate(4.5);
    while (nh.ok()){

        has_c920 = false;
        has_c270 = false;

        ros::spinOnce();



        transformStamped.header.stamp = ros::Time::now();
        transformStamped.header.frame_id = "world";
        transformStamped.child_frame_id = "my_id";

        if (has_c270 & has_c920){

            if ((pose_c920.position.x < -1.0) & (pose_c270.position.x < -1.0)){
                has_c270 = false;
                ROS_INFO ("regiao da c920");
            }
            else if ((pose_c920.position.x > 0.0) & (pose_c270.position.x > 0.0)){
                has_c920 = false;
                ROS_INFO ("regiao da c270");
            }
            else{
                ROS_INFO ("regiao das duas");


            transformStamped.transform.translation.x = (pose_c270.position.x + pose_c920.position.x)*0.5;
            transformStamped.transform.translation.y = (pose_c270.position.y + pose_c920.position.y)*0.5;
            transformStamped.transform.translation.z = (pose_c270.position.z + pose_c920.position.z)*0.5;

            tf2::Quaternion q_c270, q_c920, q;
            q_c270 = tf2::Quaternion(pose_c270.orientation.x,pose_c270.orientation.y,pose_c270.orientation.z,pose_c270.orientation.w);
            q_c920 = tf2::Quaternion(pose_c920.orientation.x,pose_c920.orientation.y,pose_c920.orientation.z,pose_c920.orientation.w);
            q=q_c270.slerp(q_c920,0.5);

            transformStamped.transform.rotation.x = q.x();
            transformStamped.transform.rotation.y = q.y();
            transformStamped.transform.rotation.z = q.z();
            transformStamped.transform.rotation.w = q.w();

            br.sendTransform(transformStamped);
            }
        }
        if (has_c270 & !has_c920){
            ROS_INFO ("so 270");
            transformStamped.transform.translation.x = pose_c270.position.x;
            transformStamped.transform.translation.y = pose_c270.position.y;
            transformStamped.transform.translation.z = pose_c270.position.z;

            transformStamped.transform.rotation.x = pose_c270.orientation.x;
            transformStamped.transform.rotation.y = pose_c270.orientation.y;
            transformStamped.transform.rotation.z = pose_c270.orientation.z;
            transformStamped.transform.rotation.w = pose_c270.orientation.w;
            br.sendTransform(transformStamped);
        }
        else if(!has_c270 & has_c920){
ROS_INFO ("so 920");
            transformStamped.transform.translation.x = pose_c920.position.x;
            transformStamped.transform.translation.y = pose_c920.position.y;
            transformStamped.transform.translation.z = pose_c920.position.z;

            transformStamped.transform.rotation.x = pose_c920.orientation.x;
            transformStamped.transform.rotation.y = pose_c920.orientation.y;
            transformStamped.transform.rotation.z = pose_c920.orientation.z;
            transformStamped.transform.rotation.w = pose_c920.orientation.w;
            br.sendTransform(transformStamped);
        }
        rate.sleep();
    }



}
