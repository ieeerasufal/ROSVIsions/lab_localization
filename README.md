# lab_localization

## 1. Introdução
O pacote lab_localization utiliza o pacote do ROS ar_track_alvar, além de múltiplas câmeras para estimar a posição e orientação de marcadores e com isso realizar o mapeamento do laboratório e o rastreamento de objetos no mapa.

## 2. Metodologia

Atualmente o laboratório de robótica dispõe de dois modelos câmeras, uma Logitec C270 e uma Logitec C920. Cada câmera foi calibrada individualmente e posicionada em lugares específicos do laboratório. As informações de calibração se encontram na pasta [yaml/](https://gitlab.com/ieeerasufal/ROSVIsions/lab_localization/tree/master/yaml).

Um marcador Alvar foi utilizado no meio da sala para servir de referencial comum para ambas as câmeras, o procedimento está descrito na wiki. Através desse referencial e da posição conhecida das câmeras é possível estimar a posição de outros objetos no campo de visão das câmeras, gerando assim um mapa.

## 3. Rodando o pacote

Há três formas de rodar o pacote.

##### 3.1 Apenas com a Logitec C270

> $ roslaunch lab_localization c270.launch

##### 3.2 Apenas com a Logitec C920

> $ roslaunch lab_localization c920.launch

##### 3.3 Com as duas câmeras

> $ roslaunch lab_localization dual.launch

## 4. To Do List

* Filtro de média para estimar a posição de múltiplas câmeras.